#!/bin/sh

SUITE=$1
NONFREE=$2

if [ "$SUITE" = "testing" ]; then
    COMPONENTS="main"
    [ "$NONFREE" = "true" ] && COMPONENTS="$COMPONENTS non-free"
    echo "deb http://security.debian.org/debian-security $SUITE-security $COMPONENTS" >> /etc/apt/sources.list
fi