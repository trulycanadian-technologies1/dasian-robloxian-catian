#!/bin/sh

# Setup hostname
echo $1 > /etc/hostname
echo export ETNA_MESA_DEBUG=no_supertile >> /etc/profile.etnaviv.sh
# Setup Dasian Theme Later

# Load phosh on startup if package is installed
#if [ -f /usr/bin/phosh ]; then
#    systemctl enable phosh.service
#fi

